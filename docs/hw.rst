###########################
Chip card
###########################

************************
Pad Description
************************

The pads are at bottom of the chip.
Table presents description of pads. Pin1 (VSSA) is the right most pad and Pin48 (subpix) is the left most pad.

.. csv-table:: Table: Pad List
    :file: _static/pinlist.csv
    :header-rows: 1


Wire bonding plan:

 .. image:: _static/wirebonding.png
       :width: 400

PCB (HVMAPS_v2):

    xxx xxxx.pdf
    
****************************************
Power supplies and reference voltages
****************************************

Clicfast2023 requires several external inputs

.. csv-table::
    :file: _static/connections.csv
    :header-rows: 1

VSS:

    VSS should be able to drain current. Connect to a power source (TOELLNER only) as follows:.

  .. image:: _static/vss.png

INJ_MON (optional):

    This signal can be used as an external trigger input of a pulse generator. 
    It is assigned to Pin7 of Connector-JB (Nexys) or Connector JC (Genesys2).

TRIG_IN (optional):

    This is a trigger input. It can be connected from a external pulse generator or scintillator
    It is assigned to Pin10 of Connector-JB (Nexys) or Connector JC (Genesys2).

 .. image:: _static/jb.png
       :width: 400






