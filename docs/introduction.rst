############
Introduction
############

Clicfast2023 is a prototype chip is a high-time-resolution detector. Each pixel has 12 comparators

Chip summary
===========================

CMOS Process:

  - TSI 180 nm process (run2023)

Wafer resistivity and chip size:

  - High resistive wafer 370 ohm/cm2
  - Chip size: 5 mm x 2 mm
  - Pixel size:  um x  um
  - Number of pixels: 29 columns x 1rows

Readout electronics:


  - 12 comparators / pixel (124 hit buffers / column)
  - 2-phase shift registers for pixel, global DAC configuration
  - triggerless readout
  - hit data with bit Time-of-Arrival and bits Time-over-Threshod in 40 MHz timestamp
  - 8b10b data stream


Revisions
========================

v1.0: 2023-02-14

  - Created

