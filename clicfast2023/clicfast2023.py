#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#

import os
import time
import logging
import yaml
import numpy as np

from basil.dut import Dut

import pkg_resources
VERSION = pkg_resources.get_distribution("clicfast2023-daq").version
OUTPUT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + os.sep + 'output'


def mk_fname(ext="data.npy", dirname=None):
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(OUTPUT_DIR, prefix)
    if not os.path.exists(dirname):
        os.system("mkdir -p {0:s}".format(dirname))
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)


class Clicfast2023(Dut):

    def __init__(self, conf=None, **kwargs):

        # set logger
        self.logger = logging.getLogger()
        logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] (%(threadName)-10s) %(message)s")
        fname = mk_fname(ext="clicfast2023.log")
        fileHandler = logging.FileHandler(fname)
        fileHandler.setFormatter(logFormatter)
        self.logger.addHandler(fileHandler)
        self.conf_buf = []
       
        # prepare some constants and memories
        self.ncols = 29
        self.ncomps = 12
        self.PixelConf = {'en_comp': np.ones([self.ncols, self.ncomps], dtype='u1') * 0xFF,
                          'tdac': np.ones([self.ncols, self.ncomps], dtype='u1') * 0xFF,
                          }

        if conf is None:
            conf = os.path.dirname(os.path.abspath(__file__)) + os.sep + "clicfast2023.yml"
        if isinstance(conf, str):
            with open(conf) as f:
                conf = yaml.safe_load(f)  # TODO (TH) use safe_load
        super(Clicfast2023, self).__init__(conf=conf)

    def init(self):
        super(Clicfast2023, self).init()
        fw_version = self.get_fw_version()
        logging.info("Firmware version: {0}".format(fw_version))
        self['INJ_BOARD'].set_size(self["INJ_BOARD"]._conf['size'])
        self['CONF'].set_size(self["CONF"]._conf['size'])
        self.reset_chip()
        self['sync'].WIDTH = 1
        self['sync'].DELAY = 2**24 - 1
        self['sync'].REPEAT = 0

    def reconnect(self):
        try:
            self['intf'].close()
        except Exception:
            self.logger.warn('reconnect: port close failed')
        self['intf'].init()

    def get_fw_version(self):
        return self['intf'].read(0x10000, 1)[0]

    def reset_chip(self):
        # Rst chip
        self['SW'].set_configuration(self['SW']._init)
        self['SW']['RstAnalogB'] = 0
        self['SW'].write()
        self['SW']['RstAnalogB'] = 1
        self['SW'].write()
        # Initialize CONF
        self['CONF'].set_configuration(self['CONF']._init)
        en_comp = np.copy(self.PixelConf['en_comp'])
        en_comp[:, :] = 0
        tdac = np.copy(self.PixelConf['tdac'])
        tdac[:, :] = 7
        self.set_ram(en_comp, tdac)
        # self.logger.info('reset_chip: init conf and pixel-RAM')

    def set_conf(self, init=False, write=True, **kwargs):
        log = 'set_conf:'
        if init:
            self['CONF'][:] = 0
            self['CONF'].set_configuration(self['CONF']._init)
            log = log + 'init'

        for k in kwargs:
            if k == 'PixelConf':
                for i, pc in enumerate(kwargs[k]):
                    for p_k, p_v in pc.items():
                        self['CONF']['PixelConf'][i][p_k] = p_v
            else:
                if isinstance(kwargs[k], str):
                    kwargs[k] = int(kwargs[k], 2)
                self['CONF'][k] = kwargs[k]
            log = log + '{}={}'.format(k, kwargs[k])

        for c in range(self.ncols):
            self['CONF']['PixelConf'][c]['WrRAM'] = 0x0

        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        self.logger.info(log)

    def set_inj(self, inj_n=100, inj_delay=(2**9), inj_width=(2**9), ext=True,
                to_inj_board=True, to_chip_trigger=False):

        """ Sets parameters for injections

        Parameters
        ----------
        inj_n : int
            number of injection pulses. 0 is infinit
        inj_delay : int optional
            delay of injection in clk of puls_gen
        inj_width : int optional
            delay of injection in clk of puls_gen
        ext : bool optional
            enable external trigger, which is connected to "gate"
        to_inj_board: bool optional
            True: sends pulse to the injection board
        to_chip_trigger: bool optional
            True: sends pulse to "Trigger" pad of the chip

        Returns
        -------
        None

        """

        self['SW']['EN_CHOPPER'] = to_inj_board
        self['SW']['EN_Trigger'] = to_chip_trigger
        self['SW'].write()

        self['inj'].reset()
        self['inj']["REPEAT"] = inj_n
        self['inj']["DELAY"] = inj_delay
        self['inj']["WIDTH"] = inj_width
        self['inj']["EN"] = ext
        self['gate'].REPEAT = 1
        self['gate'].WIDTH = self['sync'].WIDTH + self['sync'].DELAY
        self['gate'].DELAY = 1
        if ext:
            self['sync'].start()

        self.logger.info("set_inj: inj_width={0:d} inj_delay={1:d} inj_n={2:d} ext={3:d}".format(
            inj_width, inj_delay, inj_n, int(ext)))

    def set_inj_phase(self, phase):
        '''
        Parameters
        ----------
        phase : int
            phase of injection
        '''
        period = int(self['inj']["DELAY"] + self['inj']["WIDTH"])
        phase_delay = (self['inj']["DELAY"] & 0xFFFF_F000) + int(phase // 32)
        self['inj']["DELAY"] = phase_delay
        self['inj']["WIDTH"] = period - phase_delay
        self['inj'].set_phase(int(phase & 0x1F))
        self.logger.info("set_phase: width=0x{0:x} delay=0x{1:x} phase={2:d}".format(
            self['inj'].WIDTH,
            self['inj'].DELAY,
            self['inj'].FINE_DELAY_VALUE))

    def set_en_ampout(self, cols='none', write=True):
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):  # if 'none', disable all
            cols = []
        for col in range(self.ncols):
            if col in cols:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_ampout_col'] = 1
            else:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_ampout_col'] = 0
            self['CONF']['PixelConf'][self.ncols - 1 - col]['WrRAM'] = 0x0  # be sure all WrRAM are 0

        en_ampout_col = 0
        for c in range(self.ncols):
            en_ampout_col = (en_ampout_col << 1) | self['CONF']['PixelConf'][c]['en_ampout_col'].tovalue()

        log = 'set_en_ampout: en_ampout_col=0x{0:08x}'.format(en_ampout_col)
        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        self.logger.info(log)

    def set_en_inj(self, cols='none', write=True):
        if isinstance(cols, str):
            if cols == 'all':
                cols = range(self.ncols)
            elif cols == 'none':
                cols = []
        elif isinstance(cols, int):
            cols = np.array([cols])

        for col in range(self.ncols):
            if col in cols:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'] = 1
            else:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'] = 0
            self['CONF']['PixelConf'][self.ncols - 1 - col]['WrRAM'] = 0x0  # just in case
        # log
        en_injection_col = 0
        for c in range(self.ncols):
            en_injection_col = (en_injection_col << 1) | self['CONF']['PixelConf'][c]['en_injection_col'].tovalue()

        log = 'set_en_inj: en_injection_col=0x{0:08x}'.format(en_injection_col)
        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        self.logger.info(log)

    def get_en_inj(self):
        cols = []
        for col in range(self.ncols):
            if self['CONF']['PixelConf'][self.ncols - 1 - col]['en_injection_col'].tovalue() == 1:
                cols.append(col)
        return cols

    def set_en_hitbus(self, cols, write=True):
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):
            if cols == 'none':
                cols = []
            else:   # if 'all', enable all
                cols = range(self.ncols)
        for col in range(self.ncols):
            if col in cols:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_hitbus_col'] = False
            else:
                self['CONF']['PixelConf'][self.ncols - 1 - col]['en_hitbus_col'] = True
            self['CONF']['PixelConf'][self.ncols - 1 - col]['WrRAM'] = 0x0  # # just in case
    
        # log
        en_hitbus_col = 0
        for c in range(self.ncols):
            en_hitbus_col = (en_hitbus_col << 1) | self['CONF']['PixelConf'][c]['en_hitbus_col'].tovalue()
        log = 'set_en_hitbus: en_hitbus_col=0x{0:08x}'.format(en_hitbus_col)

        self['CONF'].write()
        for i in range(1000):
            time.sleep(0.002)
            if self['CONF'].is_ready:
                break
        self.logger.info(log)

    def set_en_comp(self, pix, write=True):
        en_comp = np.copy(self.PixelConf['en_comp'][:, :])
        if isinstance(pix, str):
            if pix == 'all':
                en_comp[:, :] = 1
            elif pix == 'none':
                en_comp[:, :] = 0
        elif isinstance(pix[0], int):
            en_comp[:, :] = 0
            en_comp[pix[0], pix[1]] = 1
        elif len(pix[0]) == 2:
            en_comp[:, :] = 0
            for p in pix:
                en_comp[p[0], p[1]] = 1
        else:
            en_comp[:, :] = pix

        if write:
            self.set_ram(en_comp=en_comp, tdac=None)

    def set_tdac(self, tdac):
        if isinstance(tdac, int):
            tdac = np.ones(self.PixelConf['tdac'].shape, dtype=np.uint8) * tdac
        self.set_ram(en_comp=None, tdac=tdac)

    def set_ram(self, en_comp, tdac):
        # #################
        # find rows to be changed
        if en_comp is not None:
            rows = np.argwhere(self.PixelConf['en_comp'][:, :] != en_comp)[:, 1]
        else:
            en_comp = np.clip(self.PixelConf['en_comp'][:, :], 0, 1)
            rows = np.ones(0, dtype=int)
        if tdac is not None:
            rows = np.append(rows, np.argwhere(self.PixelConf['tdac'][:, :] != tdac)[:, 1])
        else:
            tdac = np.clip(self.PixelConf['tdac'][:, :], 0, 7)
        rows = np.unique(rows)
        
        # RAM write
        for r in rows:
            # update RAM values
            for c in range(self.ncols):
                self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'] = 0  # reset all WrRAM=0

                self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'] = int(tdac[c, r])
                self.PixelConf['tdac'][c, r] = int(tdac[c, r])

                self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'] = int(en_comp[c, r])
                self.PixelConf['en_comp'][c, r] = int(en_comp[c, r])

            # Set WrRAM 1
            div_r, mod_r = divmod(r, 5)
            if (div_r % 2):  # odd
                # print('odd', div_r % 2)
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][4 - mod_r] = '1'
            else:  # even
                # print('even', div_r % 2)
                self['CONF']['PixelConf'][self.ncols - 1 - div_r]['WrRAM'][mod_r] = '1'

            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            # Set WrRAM 0 to fix the RAM value
            log_wr = ''
            for c in range(self.ncols):
                log_wr = "{}_{},{},{}".format(log_wr,
                                              self['CONF']['PixelConf'][self.ncols - 1 - c]['en_comp'],
                                              self['CONF']['PixelConf'][self.ncols - 1 - c]['TuneDAC'],
                                              self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'])
                self['CONF']['PixelConf'][self.ncols - 1 - c]['WrRAM'] = 0
            self['CONF'].write()
            for i in range(1000):
                time.sleep(0.002)
                if self['CONF'].is_ready:
                    break
            self.logger.info('set_ram row{} en_comp{} tdac{} wr{}'.format(r, self.PixelConf['en_comp'][:, r], self.PixelConf['tdac'][:, r], log_wr))

    def set_inj_amp(self, inj_amp, unit='V'):
        if unit == 'V':
            # VDD3V3 = 3V
            a, b, c = (1.6750507793825382e-08, 0.00013919799450240716, 0.013104904296381592)
            inj_amp = (np.sqrt(b * b - 4 * a * (c - inj_amp)) - b) / (2 * a)
        elif unit == 'bits':
            inj_amp = int(inj_amp, 2)
        inj_amp = int(inj_amp)
        if inj_amp > 9830:
            self.logger.warn('set_inj_amp: fail to set inj_amp={0:d}DAU, inj_amp must be 0-1.8V(9830DAU)'.format(inj_amp))
            inj_amp = 9830
        elif inj_amp < 0:
            self.logger.warn('set_inj_amp: fail to set inj_amp={0:d}DAU, inj_amp must be 0-1.8V(9830DAU)'.format(inj_amp))
            inj_amp = 0
        self['INJ_BOARD']['AMP'] = inj_amp
        self['INJ_BOARD']['LOAD'] = 0
        self['INJ_BOARD'].write()
        for i in range(1000):
            if self['INJ_BOARD'].is_ready:
                break
            else:
                time.sleep(0.001)
        self['INJ_BOARD']['LOAD'] = 1
        self['INJ_BOARD'].write()
        while not self['INJ_BOARD'].is_ready:
            time.sleep(0.001)

        self.logger.info("inj_amp: {0}".format(inj_amp))

    def inject(self, wait=False):
        if self['inj'].EN:
            self["gate"].start()
            while (not self["gate"].is_ready) and wait:
                time.sleep(0.001)
        else:
            self['inj'].start()
            while (not self['inj'].is_ready) and wait:
                time.sleep(0.001)

    def get_data_now(self):
        return self['fifo'].get_data()

    def get_data(self, timeout=1, n_ts=None, inj='inj'):
        # start injection
        self['data_rx'].set_en(True)
        if self[inj].EN:
            self["gate"].start()
        else:
            self[inj].start()
        if n_ts is None:
            n_ts = self[inj].REPEAT
        nloops = int(timeout // 0.01)

        # get data
        raw = self['fifo'].get_data()
        for i in range(nloops):
            time.sleep(0.01)
            raw = np.append(raw, self['fifo'].get_data())
            if n_ts != 0 or self[inj].is_ready:
                ts_len = (raw & 0xE000_0000 == 0x8000_0000).sum()
                break
        for j in range(i, nloops):
            time.sleep(0.01)
            tmp_raw = self['fifo'].get_data()
            raw = np.append(raw, tmp_raw)
            if n_ts != 0:
                ts_len = ts_len + (tmp_raw & 0xE000_0000 == 0x8000_0000).sum()
                # print(i, ts_len, n_ts)
                if ts_len >= n_ts * 3:
                    break
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # set rx off
        self['data_rx'].set_en(False)
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # check status
        rx_status = self["data_rx"].STATUS
        fifo_size = self['fifo'].FIFO_SIZE
        if rx_status & 0x8 == 0x0:
            rx_status = self["data_rx"].STATUS
        if rx_status != 0x8 or fifo_size != 0 or j == nloops - 1:
            self.logger.warn("get_data: error status=0x{0:x} fifo_size={1:d} loop={2:d} ts={3}/{4}".format(
                rx_status, fifo_size, j, ts_len, n_ts * 3))
        return raw

    def get_rx_status(self):
        self['data_rx'].RAW_DATA_WR = True
        self['data_rx'].RAW_DATA_WR = False
        ret = {'raw0': self['data_rx'].RAW_DATA0,
               'raw1': self['data_rx'].RAW_DATA1,
               'aligned': self['data_rx'].ALIGNED,
               'lost_cnt': self["data_rx"].LOST_COUNT,
               'decoder_err_cnt': self["data_rx"].DECODER_ERR_CNT,
               'data_err_cnt': self["data_rx"].DATA_ERR_CNT
               }
        ret['trig_inj_lost_cnt'] = self["ts_trig"].LOST_COUNT
        return ret

    def print_sync_status(self):
        self.logger.info('get_inj_status: sw={}'.format(self['SW'].get_configuration()))
        self.logger.info('get_inj_status: sync={} is_ready={}'.format(self['sync'].get_configuration(), self['sync'].is_ready))
        self.logger.info('get_inj_status: gate={} is_ready={}'.format(self['gate'].get_configuration(), self['gate'].is_ready))

    def set_sync(self, nbits=24, force_reset_time=0.1, auto_sync=True):
        ''' this function does:
               1. sends a sync-reset manually
               2. set automatic sync-reset and starts it
            nbits(int) : number of bits to sync
            wait(float): pluse length (sec) of the manual sync-reset, if negative, sync-reset kept HIGH
            example:
        '''
        # send manula sync
        if force_reset_time != 0:
            self['SW']['SYNC_RES'] = 1
        self['SW']['EN_PULSE_SYNC'] = 0
        self['SW'].write()
        # configure automatic reset
        self['sync'].REPEAT = 0
        self['sync'].DELAY = 2**nbits - 1
        self['sync'].WIDTH = 1
        self['sync'].start()
        if force_reset_time > 0:
            time.sleep(force_reset_time)
            self['SW']['EN_PULSE_SYNC'] = auto_sync
            self['SW']['SYNC_RES'] = 0
            self['SW'].write()
        self['sync'].start()

        self.logger.info('set_sync: sync_period=0x{0:x} repeat={1} auto-sync={2} manual-sync={3}'.format(
            self['sync'].DELAY + self['sync'].WIDTH,
            self['sync'].REPEAT,
            self['SW']['EN_PULSE_SYNC'],
            self['SW']['SYNC_RES']))

    def set_rx(self, en=True, edge=0, reset_wait=0.1):
        self['SW']['EN_CkExt'] = en
        self['SW']['SYNC_RES'] = 1
        self['SW'].write()
        if en:
            self['data_rx'].reset()
            self['data_rx'].SAMPLING_EDGE = edge
            if self['fifo'].FIFO_SIZE != 0:  # Clear fifo
                self.logger.info('set_rx: FIFO is not empty({}), try to discard the data...'.format(self['fifo'].FIFO_SIZE))
                self['fifo'].get_data()
                if self['fifo'].FIFO_SIZE != 0:
                    self.logger.warn('set_rx: failed to empty FIFO({0})'.format(self['fifo'].FIFO_SIZE))
            cnt0 = int(reset_wait / 0.010)
            cnt = 0
            for i in range(cnt0 * 10):  # aligne data
                time.sleep(0.010)
                if self['data_rx'].ALIGNED:
                    cnt = cnt + 1
                if cnt > cnt0:
                    break
            if i == cnt0 * 10 - 1:
                self.logger.warn('set_rx: FPGA cannot align data: aligned={0}'.format(self['data_rx'].ALIGNED))
            else:
                self.logger.info('set_rx: FPGA alined: aligned={0}'.format(self['data_rx'].ALIGNED))

            self['SW']['SYNC_RES'] = 0
            self['SW'].write()
            self['sync'].START = 1
        # self['data_rx'].set_en(en)

    def mask_rx(self, mask=True):
        self['data_rx'].set_en(not mask)
        if not mask:
            trash = self['fifo'].FIFO_SIZE
            if trash != 0:
                self['fifo'].get_data()
                self.logger.warn('mask_rx: discard trash ({0} {1})'.format(trash, self['fifo'].FIFO_SIZE))
        # self.logger.info('mask_rx: rx_en={}'.format(self['data_rx'].EN))

    def set_ts_inj(self, en=True, reset=False):
        if en:
            self['ts_inj'].RESET = reset
        self['ts_inj'].EXT_TIMESTAMP = 1
        self['ts_inj'].ENABLE = en
        self.logger.info('set_ts_inj:{0:d}'.format(en))

    def set_ts_trig(self, en=True, reset=False):
        if en:
            self['ts_trig'].RESET = reset
        self['ts_trig'].ENABLE = en
        self.logger.info('set_ts_trig:{0:d}'.format(en))

    def get_configuration(self):
        ret = super(Clicfast2023, self).get_configuration()
        ret['PixelConf'] = {'tdac': self.PixelConf['tdac'].tolist(),
                            'en_comp': self.PixelConf['en_comp'].tolist()}
        # add some statuses
        ret['status'] = self.get_rx_status()
        ret['status']['inj_ready'] = self['inj'].READY
        ret['status']['sync_ready'] = self['sync'].READY
        ret['status']['gate_ready'] = self['gate'].READY
        ret['status']['timestamp'] = self['ts_trig'].TIMESTAMP
        return ret

    def save_config(self, config_file=None):
        if config_file is None:
            config_file = mk_fname(ext="config.yaml", dirname=None)
        with open(config_file, 'w') as f:
            yaml.dump(self.get_configuration(), f, default_flow_style=False)
        self.logger.info('save_config: ' + config_file)
        return config_file
    
    def set_configuration(self, conf):
        pixel_conf = conf.pop('PixelConf', {'en_comp': None, 'tdac': None})
        status = conf.pop('status', None)
        super(Clicfast2023, self).set_configuration(conf)
        self.set_conf()
        self.set_ram(en_comp=np.array(pixel_conf['en_comp'], dtype=self.PixelConf['en_comp'].dtype),
                     tdac=np.array(pixel_conf['tdac'], dtype=self.PixelConf['tdac'].dtype))
        self['INJ_BOARD'].write()
        if status['inj_ready'] == 0:
            self['inj'].START = 1
        if status['sync_ready'] == 0:
            self['sync'].START = 1      
        if status['gate_ready'] == 0:
            self['gate'].START

    def load_config(self, config_file):
        with open(config_file) as f:
            config = yaml.safe_load(f)
        # gpio
        sw = config.pop('SW', None)
        if sw is not None:
            self['SW']['RstAnalogB'] = sw['RstAnalogB']
            self['SW'].write()
        else:
            self.logger.warn('load_config: config of sw is unknown')
        # spi
        conf = config.pop('CONF', None)
        if conf is not None:
            self['CONF'].set_configuration(conf)
            self.set_conf()
        else:
            self.logger.warn('load_config: config of conf is unknown')
        pixel_conf = config.pop('PixelConf', None)
        if pixel_conf is not None:
            en_comp = np.array(pixel_conf['en_comp'], dtype='u1')
            tdac = np.array(pixel_conf['tdac'], dtype='u1')
        self.set_ram(en_comp, tdac)

        inj_board = config.pop('INJ_BOARD', None)
        if inj_board is not None:
            self.set_inj_amp(inj_board['AMP'], unit='bits')
        else:
            self.logger.warn('load_config: config of inj_board is unknown')

        # pulser and tfc
        sync = config.pop('sync', None)
        if sync is not None:
            nbits = int(np.log2(sync['DELAY'] + 1))
            self.set_sync(nbits=nbits, force_reset_time=0.1, auto_sync=sw['EN_PULSE_SYNC'])
        else:
            self.logger.warn('load_config: config of sync/tfc is unknown')
        inj = config.pop('inj', None)
        if inj is not None:
            self.set_inj(inj_n=inj['REPEAT'], inj_width=inj['WIDTH'], inj_delay=inj['DELAY'], ext=inj['EN'])
            self.set_inj_phase(phase=inj['FINE_DELAY_VALUE'])
            self.inject()
        else:
            self.logger.warn('load_config: config of inj is unknown')

        ts_inj = config.pop('ts_inj', None)
        if ts_inj is not None:
            self.set_ts_inj(en=ts_inj['ENABLE'])
        else:
            self.logger.warn('load_config: unknown ts_inj')
        ts_trig = config.pop('ts_trig', None)
        if ts_trig is not None:
            self.set_ts_trig(en=ts_trig['ENABLE'])
        else:
            self.logger.warn('load_config: unknown ts_trig')

        # data_rx  #########TODO
        data_rx = config.pop('data_rx', None)
        # status = config.pop('status', None)
        if data_rx is not None:
            self.set_rx(en=sw['EN_CkExt'], edge=data_rx['SAMPLING_EDGE'])
        else:
            self.logger.warn('load_config: unknown data_rx')


if __name__ == '__main__':
    pass
