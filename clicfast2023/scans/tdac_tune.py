import time

import numpy as np

import clicfast2023.scan_base as scan_base
import clicfast2023.analysis.interpreter as interpreter
import clicfast2023.analysis.event_builder as event_builder

local_configuration = {
    "cols": np.arange(29),
    "comps": np.arange(12),
    "n_inj_col": 1,
    'n_inj_comp': 1,
    "plsgen": None,
    "disable_noninjected_pixel": True,
    # #### parameter for auto-step
    'hitth': 0.01,  # 0-1 ratio of recieved hit
    'run_mode': 'auto'
}


class TdacScan(scan_base.ScanBase):
    scan_id = "tdac_tune"

    def scan(self, **kwargs):
        """ List of kwargs
            cols: list of pixel
            comps: list of pixel
            n_mask_pix: number of pixels injected at onece
            injlist: list of inj (inj_high-inj_low)
            with_mon: get timestamp of mon (mon will be enabled)
        """
        ####################
        # get options from args
        plsgen = kwargs.pop("plsgen", None)
        if plsgen is None:
            plsgen = self.dut
            inj_type = 'inj'
            # self.inj_n = plsgen[inj_type].REPEAT
        elif isinstance(plsgen, str):
            if plsgen == 'trig':
                inj_type = 'trig'
            else:
                inj_type = 'inj'
            plsgen = self.dut
            # self.inj_n = plsgen[inj_type].REPEAT
        else:
            inj_type = 'external'
            # self.inj_n = self.dut['inj'].REPEAT

        disable_noninjected_pixel = kwargs.pop("disable_noninjected_pixel", local_configuration["disable_noninjected_pixel"])

        # ### param for tdac
        self.hitth = kwargs.pop("hitth", local_configuration['hitth'])
        # ### debug
        run_mode = kwargs.pop("run_mode", local_configuration['run_mode'])

        ####################
        # get scan params from args
        param_dtype = [("scan_param_id", "<i4"), ("timestamp", '<f8')]
        scan_param = {}
        nsteps = 1

        comps = kwargs.pop("comps", local_configuration['comps'])
        if comps is None:
            comps = np.unique(np.argwhere(self.dut.PixelConf['en_comp']==1)[:,1])
            n_inj_comp = len(comps)
        else:
            if isinstance(comps, int):
                comps = np.array([comps], dtype=np.int8)
            n_inj_comp = min(kwargs.pop("n_inj_comp", local_configuration['n_inj_comp']), len(comps))
            n_loop_comp = (len(comps) - 1) // n_inj_comp + 1
            complist = np.ones([n_loop_comp, n_inj_comp], dtype='<u2') * 0xFFFF
            for i, r in enumerate(comps):
                div, mod = divmod(i, n_inj_comp)
                complist[div, mod] = r
            param_dtype.append(("comps", "<u2", (n_inj_comp)))
            scan_param['comps'] = complist
            nsteps = nsteps * n_loop_comp
            comps = complist[0, :]

        cols = kwargs.pop("cols", local_configuration['cols'])
        if cols is None:
            cols = np.unique(np.argwhere(self.dut.PixelConf['en_comp'] == 1)[:, 0])
            n_inj_col = len(cols)
        else:
            if isinstance(cols, int):
                cols = np.array([cols], dtype=np.int8)
            n_inj_col = min(kwargs.pop("n_inj_col", local_configuration['n_inj_col']), len(cols))
            n_loop_col = (len(cols) - 1) // n_inj_col + 1
            collist = np.ones([n_loop_col, n_inj_col], dtype='<u1') * 0xFF
            for i, c in enumerate(cols):
                div, mod = divmod(i, n_inj_col)
                collist[div, mod] = c
            param_dtype.append(("cols", "<u1", (n_inj_col)))
            scan_param['cols'] = collist
            nsteps = nsteps * n_loop_col
            cols = collist[0, :]

        for k in kwargs:
            if k == 'inj':
                param_dtype.append((k, 'f4'))
            elif k == 'phase':
                param_dtype.append((k, 'u4'))
            else:
                param_dtype.append((k, '<u2'))
            scan_param[k] = kwargs[k]
            print(k, kwargs[k])
            nsteps = nsteps * len(scan_param[k])

        param_dtype.append(('tdacs', 'u1', n_inj_col * n_inj_comp))
        param_dtype.append(('en_comps', 'u1', n_inj_col * n_inj_comp))

        # save original RAM settings
        self.en_comp_org = np.copy(self.dut.PixelConf['en_comp'])
        self.tdac_org = np.copy(self.dut.PixelConf['tdac'])
        self.tdac = np.copy(self.dut.PixelConf['tdac'])
        self.en_comp = np.copy(self.dut.PixelConf['en_comp'])
        self.status = np.copy(self.dut.PixelConf['tdac']) * 0xFD  ## -3 not searched, -2 lower than tdac0,  -1 searching
        ####################
        # create a table for scan_params
        param_dtype = np.dtype(param_dtype)
        self.scan_param_table = self.h5_file.create_table(
            self.h5_file.root,
            name='scan_parameters',
            title='scan_parameters',
            description=param_dtype,
            filters=self.filter_tables)
        for k, v in scan_param.items():
            self.meta_data_table.attrs[k] = v
        current_param = {}

        ####################
        # enable readout
        self.dut.mask_rx(True)
        if inj_type == 'trig':
            ts_n = self.dut['trig']['REPEAT']
        else:
            ts_n = self.dut['inj']['REPEAT']
        with self.readout(scan_param_id=-1,                       # TODO open/close self.readout for each scan step (make it faster)
                          fill_buffer=True, clear_buffer=True,
                          reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                          readout_interval=0.001):
            dqdata = self.fifo_readout.data
            ####################
            # main scan loop
            self.logger.info('starting scan nsteps={}'.format(nsteps))

            for param_id in range(nsteps):
                # update scan parameters
                scan_div = 1
                for pname, plist in scan_param.items():
                    plen = len(plist)
                    mod = (param_id // scan_div) % plen
                    self.scan_param_table.row[pname] = plist[mod]
                    current_param[pname] = plist[mod]
                    if param_id // scan_div != (param_id - 1) // scan_div:
                        # self.logger.info(''.join(['pname=', str(pname), '=', str(plist[mod]), ' step=', str(param_id), 
                        #                          ' scan_div=', str(scan_div), ' plen=', str(plen), ' mod=', str(mod)]))
                        if pname == 'inj':
                            plsgen.set_inj_amp(plist[mod], unit='V')
                        elif pname == 'phase':
                            plsgen.set_inj_phase(plist[mod])
                        elif pname == 'cols':
                            # self.logger.info("cols {} {}".format(plist[mod], plist[mod][plist[mod] != 0xFF]))
                            cols = plist[mod][plist[mod] != 0xFF]
                            self.dut.set_en_inj(cols=cols)
                        elif pname == 'comps':
                            # self.logger.info("comps {} {}".format(plist[mod], plist[mod][plist[mod] != 0xFF]))
                            comps = plist[mod][plist[mod] != 0xFFFF]
                        else:
                            self.dut.set_conf(**{pname: plist[mod]})
                    scan_div = scan_div * plen

                ####################
                # search tdac
                next = self.init_next_status(cols, comps, disable_noninjected_pixel)
                self.dut.set_ram(self.en_comp, self.tdac)
                while next:
                    # set new scan_param_id and inject
                    self.scan_param_id = self.scan_param_id + 1
                    self.dut.mask_rx(False)
                    if inj_type == 'trig':
                        self.dut.inject_trig()
                    else:
                        self.dut.inject()

                    # count data
                    hit_len, ts_len = 0, 0
                    data = np.empty(0, dtype='uint32')
                    while True:
                        time.sleep(0.01)
                        try:
                            tmp = dqdata.popleft()[0]
                            # self.logger.info('{} {}'.format(len(tmp),tmp.dtype))
                            ts_len = ts_len + (tmp & 0xE000_0000 == 0x8000_0000).sum()
                            hit_len = hit_len + (tmp & 0xF000_0000 == 0x1000_0000).sum()
                            # self.logger.info('ts{} hit{}'.format(ts_len, hit_len))
                            data = np.append(data, tmp)
                        except IndexError:
                            if ts_len >= ts_n * 3:
                                break

                    # update scan_param_table
                    self.scan_param_table.row['scan_param_id'] = self.scan_param_id
                    self.scan_param_table.row['timestamp'] = time.time()
                    for c_i, c in enumerate(cols):
                        for r_i, r in enumerate(comps):
                            self.scan_param_table.row['tdacs'][c_i * len(comps) + r_i] = self.tdac[c, r]
                            self.scan_param_table.row['en_comps'][c_i * len(comps) + r_i] = self.tdac[c, r]
                    for i in range(c_i * len(comps) + r_i + 1, n_inj_col * n_inj_comp):
                        self.scan_param_table.row['tdacs'][i] = 0xF
                        self.scan_param_table.row['en_comps'][i] = 0xF
                    self.scan_param_table.row.append()
                    self.scan_param_table.flush()
                    # prepare for next table data
                    for k, v in current_param.items():
                        self.scan_param_table.row[k] = v

                    self.dut.mask_rx(True)
                    next = self.next_status(data, cols, comps)
                    self.logger.info('ThScan: step={0} id={1} st={2} ts={3} hit{4}'.format(param_id, self.scan_param_id, next, ts_len, hit_len))
                    self.dut.set_ram(self.en_comp, self.tdac)
        self.h5_file.create_array(self.h5_file.root, 'tuned_tdac', self.status, 'tuned tdac values')
        self.tdac[self.status < 8] = self.status[self.status < 8]
        self.tdac[self.status == 0xFD] = self.tdac_org[self.status == 0xFD]
        self.tdac[self.status == 0xFE] = 0
        
        self.dut.set_ram(self.en_comp_org, self.tdac)

    def init_next_status(self, cols, comps, disable_noninjected_pixel):
        if disable_noninjected_pixel:
            self.en_comp[:, :] = 0
            self.en_comp[cols, comps] = 1
        else:
            self.en_comp[:, :] = self.en_comp_org[:, :]
        self.tdac[:, :] = self.tdac_org[:, :]

        self.status[cols, comps] = 0xFF
        self.status[((self.status == 0xFF) & (self.en_comp == 1))] = 0xFF
        return np.any(self.status == 0xFF)

    def next_status(self, data, cols, comps):
        '''
        status 0xFF searching
               0xFE lower than 0
               0xFD not tuned
               >=0  tuned TDAC value
        '''
        hit, ts, ts_trig = interpreter.raw2list(data)
        for c in cols:
            for r in comps:
                cnt = ((hit['col'] == c) & (hit['comp'] == r)).sum()
                log = "pix{}-{} cnt={} tdac={}".format(c, r, cnt, self.tdac[c, r])
                if self.status[c, r] == 0xFF:
                    if cnt >= self.hitth * (len(ts) + len(ts_trig)):
                        self.status[c, r] = self.tdac[c, r]
                        self.en_comp[c, r] = 0
                    elif self.tdac[c, r] == 0:
                        self.status[c, r] = 0xFE
                        self.en_comp[c, r] = 0
                    else:
                        self.tdac[c, r] = self.tdac[c, r] - 1
                self.logger.info('{0} --> tdac={1} en_comp={2} st=0x{3:x}'.format(
                    log,
                    self.tdac[c, r],
                    self.en_comp[c, r],
                    self.status[c, r]))

        return np.any(self.status == 0xFF)

    def analyze(self):
        fraw = self.output_filename + '.h5'
        fhit = fraw[:-7] + 'hit.h5'

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))
        return fhit

    def plot(self):
        fraw = self.output_filename + '.h5'
        fhit = self.output_filename[:-4] + 'hit.h5'
        fpdf = self.output_filename + '.pdf'


if __name__ == "__main__":

    from clicfast2023.clicfast2023 import Clicfast2023
    dut = Clicfast2023()
    dut.init()
    dut.set_en_comp('all')
    # dut.set_sync(inj)
    dut.set_inj(inj_n=100, ext=True)
    dut.set_inj_amp(0.1, unit='V')

    scan = TdacTune(dut, online_monitor_addr="")
    scan.start(**local_configuration)
    scan.analyze()
    # scan.plot()
