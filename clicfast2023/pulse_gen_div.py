#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer


class pulse_gen_div(RegisterHardwareLayer):
    '''Pulser generator
    '''

    _registers = {'RESET': {'descr': {'addr': 0, 'size': 8, 'properties': ['writeonly']}},
                  'VERSION': {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},
                  'START': {'descr': {'addr': 1, 'size': 8, 'properties': ['writeonly']}},
                  'CLKDV': {'descr': {'addr': 1, 'size': 8, 'properties': ['ro']}},
                  'READY': {'descr': {'addr': 2, 'size': 1, 'offset': 0, 'properties': ['ro']}},
                  'EN': {'descr': {'addr': 2, 'size': 1, 'offset': 1}},
                  'EN_OUT': {'descr': {'addr': 2, 'size': 6, 'offset': 2, 'default': 0x3F}},
                  'DELAY': {'descr': {'addr': 3, 'size': 32}},
                  'WIDTH': {'descr': {'addr': 7, 'size': 32}},
                  'REPEAT': {'descr': {'addr': 11, 'size': 32}},
                  'PHASE_DES': {'descr': {'addr': 16, 'size': 8}},
                  'DEBUG': {'descr': {'addr': 17, 'size': 64}},
                  }
    _require_version = "==1"

    def __init__(self, intf, conf):
        super(pulse_gen_div, self).__init__(intf, conf)
        self._clkdv = self.CLKDV

    def start(self):
        self.START = 0

    def reset(self):
        self.RESET = 0

    def set_delay(self, value):
        self.DELAY = value

    def get_delay(self):
        return self.DELAY

    def set_phase(self, value):
        phase_des = 0
        for i in range(value):
            phase_des = phase_des | (1 << i)
        self.PHASE_DES = phase_des

    def get_phase(self):
        phase_des = self.PHASE_DES
        for i in range(self._clkdv * 4):
            if (phase_des >> i) & 0x1 == 0x0:
                break
        return i

    def set_width(self, value):
        self.WIDTH = value

    def get_width(self):
        return self.WIDTH

    def set_repeat(self, value):
        self.REPEAT = value

    def get_repeat(self):
        return self.REPEAT

    def is_done(self):
        return self.is_ready

    @property
    def is_ready(self):
        return self.READY

    def set_en(self, value):
        '''
        If true: The pulse comes with a fixed delay with respect to the external trigger (EXT_START).
        If false: The pulse comes only at software start.
        '''
        self.EN = value

    def get_en(self):
        '''
        Return info if pulse starts with a fixed delay w.r.t. shift register finish signal (true) or if it only starts with .start() (false)
        '''
        return self.EN
