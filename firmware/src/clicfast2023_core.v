`timescale 1ps / 1ps

module clicfast2023_core(
//local bus
    input wire BUS_CLK,
    inout wire [7:0] BUS_DATA,
    input wire [15:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    input wire BUS_RST,
//clocks
    input wire CLK20,
    input wire CLK40,
    input wire CLK100,
    input wire CLK200,
    input wire CLK400,
//fifo
    input wire ARB_READY_OUT,
    output wire ARB_WRITE_OUT,
    output wire [31:0] ARB_DATA_OUT,
    input wire FIFO_FULL,
    input wire FIFO_NEAR_FULL,
// Chip
    output wire RstAnalogB, //connected to reset_analog_b...
    // data output stream
    output wire CkExt,
    output wire CkRef,
    output wire SyncRes,
    output wire Trigger,
    input wire Data,
    // shift registers
    output wire CSIn,
    output wire CCk1,
    output wire CCk2,
    output wire CRb,
    output wire CLd,
    input wire CSOut,
// PCB
    output wire INJ_CHOPPER,
    output wire INJ_MON,
    output wire INJ_LOOP,
    input wire TRIG_IN, // INJ_LOOPBACK
    output wire GECCO_SCLK,
    output wire GECCO_SDI,
    output wire GECCO_SLD,
// LED, trigger, Btn, etc
    output wire [7:0] LED
);

// -------  MODULE ADREESSES  ------- //
localparam SPI_INJ_BASEADDR = 16'h0010;
localparam SPI_INJ_HIGHADDR = 16'h0080-1;

localparam SPI_VOLT_BASEADDR = 16'h0080;
localparam SPI_VOLT_HIGHADDR = 16'h0100-1;

localparam PULSE_INJ_BASEADDR = 16'h0100; // addr width 33 incl. debug
localparam PULSE_INJ_HIGHADDR = 16'h0140-1;

localparam PULSE_SYNC_BASEADDR = 16'h0140; //addr-width = 15 
localparam PULSE_SYNC_HIGHADDR = 16'h0180-1;

localparam PULSE_GATE_BASEADDR = 16'h0180; //addr-width = 15 
localparam PULSE_GATE_HIGHADDR = 16'h01C0-1;

localparam TS_INJ_BASEADDR = 16'h01C0;  //addr-width = 12
localparam TS_INJ_HIGHADDR = 16'h0200-1;

localparam GPIO_BASEADDR = 16'h0200;
localparam GPIO_HIGHADDR = 16'h0280-1;

localparam TS_TRIG_BASEADDR = 16'h0280;  //addr-width = 12
localparam TS_TRIG_HIGHADDR = 16'h02C0-1;

//localparam PULSE_TRIG_BASEADDR = 16'h02C0;  //addr-width = 12
//localparam PULSE_TRIG_HIGHADDR = 16'h0300-1;

localparam CLICFAST_RX_BASEADDR = 16'h0380;
localparam CLICFAST_RX_HIGHADDR = 16'h0400-1;

localparam SPI_CONF_BASEADDR = 16'h0400;
localparam SPI_CONF_HIGHADDR = 16'h0600-1;

// -------  FPGA VERSION ------- //
localparam VERSION = 8'h03;
reg RD_VERSION;
always@(posedge BUS_CLK)
    if(BUS_ADD == 16'h0000 && BUS_RD)
        RD_VERSION <= 1;
    else
        RD_VERSION <= 0;
assign BUS_DATA = (RD_VERSION) ? VERSION : 8'bz;

// ------- Data Stream ------- //
wire RX_FIFO_READ,RX_FIFO_EMPTY;
wire [31:0] RX_FIFO_DATA;
wire TS_INJ_FIFO_READ, TS_INJ_FIFO_EMPTY;
wire [31:0] TS_INJ_FIFO_DATA;
wire TS_TRIG_FIFO_READ, TS_TRIG_FIFO_EMPTY;
wire [31:0] TS_TRIG_FIFO_DATA;

rrp_arbiter #( 
    .WIDTH(3)
) rrp_arbiter (
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    .WRITE_REQ({ ~RX_FIFO_EMPTY, ~TS_INJ_FIFO_EMPTY, ~TS_TRIG_FIFO_EMPTY}),
    .HOLD_REQ(3'b0),
    .DATA_IN({RX_FIFO_DATA, TS_INJ_FIFO_DATA, TS_TRIG_FIFO_DATA}),
    .READ_GRANT({RX_FIFO_READ, TS_INJ_FIFO_READ, TS_TRIG_FIFO_READ}),
    .READY_OUT(ARB_READY_OUT),
    .WRITE_OUT(ARB_WRITE_OUT),
    .DATA_OUT(ARB_DATA_OUT)
);
// ------- GECCO ------- //
// injection
wire SPI_CLK;
//assign SPI_CLK = CLK20;
clock_divider #(
      .DIVISOR(20)    //200=100kHz
  ) clock_divider_spi (
      .CLK(CLK20),
      .RESET(BUS_RST),
      .CLOCK(SPI_CLK)
  );

spi #( 
    .BASEADDR(SPI_INJ_BASEADDR), 
    .HIGHADDR(SPI_INJ_HIGHADDR),
    .MEM_BYTES(5)
) spi_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(SPI_CLK),

    .SCLK(GECCO_SCLK),
    .SDI(GECCO_SDI),
    .SDO(1'b0),
    .EXT_START(1'b0),
    .SEN(),
    .SLD(GECCO_SLD)
);

wire INJ_EXT_START;
// pulse_gen #( 
//     .BASEADDR(PULSE_INJ_BASEADDR),
//     .HIGHADDR(PULSE_INJ_HIGHADDR)
// ) pulse_gen_inj (
//     .BUS_CLK(BUS_CLK),
//     .BUS_RST(BUS_RST),
//     .BUS_ADD(BUS_ADD),
//     .BUS_DATA(BUS_DATA),
//     .BUS_RD(BUS_RD),
//     .BUS_WR(BUS_WR),

//     .PULSE_CLK(CLK100),
//     .EXT_START(INJ_EXT_START),
//     .PULSE(INJ_CHOPPER)
// );
wire PULSE_DLY;
pulse_gen_delay #( 
    .BASEADDR(PULSE_INJ_BASEADDR),
    .HIGHADDR(PULSE_INJ_HIGHADDR)
) pulse_gen_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK400),  //TODO do you
    .PULSE_CLKDLY(CLK200),
    .EXT_START(INJ_EXT_START),
    .PULSE(),
    .PULSE_DLY(PULSE_DLY)
);
wire EN_Trigger, EN_CHOPPER;
assign Trigger = EN_Trigger ? PULSE_DLY:1'b0;
assign INJ_MON = PULSE_DLY;
assign INJ_LOOP = PULSE_DLY;
assign INJ_CHOPPER = EN_CHOPPER ? PULSE_DLY:1'b0;

wire [63:0] TIMESTAMP;
timestamp #(
    .BASEADDR(TS_INJ_BASEADDR),
    .HIGHADDR(TS_INJ_HIGHADDR),
    .IDENTIFIER(8)
    //.CLKDV(2),    //CLKW x CLKDV = CLK  40MHz x 5 = 200MHz
    //.DIV_WIDTH(3) // no of bits of (CLKDIV * 4) = 20
) timestamp_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .CLK(CLK100),
    .DI(PULSE_DLY),
    .TIMESTAMP_OUT(), //timestamp based on CLKW
    .EXT_TIMESTAMP(TIMESTAMP),
    .EXT_ENABLE(1'b0),

    .FIFO_READ(TS_INJ_FIFO_READ),
    .FIFO_EMPTY(TS_INJ_FIFO_EMPTY),
    .FIFO_DATA(TS_INJ_FIFO_DATA)
);

timestamp_div #(
    .BASEADDR(TS_TRIG_BASEADDR),
    .HIGHADDR(TS_TRIG_HIGHADDR),
    .IDENTIFIER(9),
    .CLKDV(2),    //CLKW x CLKDV = CLK  40MHz x 5 = 200MHz
    .DIV_WIDTH(3) // no of bits of (CLKDIV * 4) = 20
) timestamp_div_trig ( ///master timestamp
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .CLK2x(CLK400),
    .CLK(CLK200),
    .CLKW(CLK100),
    .DI(PULSE_DLY),
    .TIMESTAMP_OUT(TIMESTAMP), //timestamp based on CLKW
    .EXT_TIMESTAMP(64'b0),
    .EXT_ENABLE(1'b0),

    .FIFO_READ(TS_TRIG_FIFO_READ),
    .FIFO_EMPTY(TS_TRIG_FIFO_EMPTY),
    .FIFO_DATA(TS_TRIG_FIFO_DATA),

    .FIFO_READ_FALL(1'b0),
    .FIFO_EMPTY_FALL(),
    .FIFO_DATA_FALL()
);

wire PULSE_SYNC, SYNC_RES, EN_PULSE_SYNC;
pulse_gen #( 
    .BASEADDR(PULSE_SYNC_BASEADDR), 
    .HIGHADDR(PULSE_SYNC_HIGHADDR)
) pulse_gen_sync (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .PULSE_CLK(~CLK100),
    .EXT_START(TIMESTAMP[23]),
    .PULSE(PULSE_SYNC)
);
assign SyncRes = (PULSE_SYNC & EN_PULSE_SYNC) | SYNC_RES;
wire PULSE_GATE;
assign INJ_EXT_START = PULSE_GATE & PULSE_SYNC;
pulse_gen #( 
    .BASEADDR(PULSE_GATE_BASEADDR), 
    .HIGHADDR(PULSE_GATE_HIGHADDR)
) pulse_gen_gate (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .PULSE_CLK(CLK100),
    .PULSE(PULSE_GATE)
);

//voltage selection on FMC:

// ------- Chip ------- //
wire [2:0] debug;
wire EN_CkExt, EN_CkRef;
assign CkExt = EN_CkExt ? CLK100 : 1'b0;  // this should be faster, but the PCB-FPGA does not allow to be faster....
assign CkRef = EN_CkRef ? CLK100 : 1'b0;  //this was 100MHz in GECCO
clicfast2023_rx #(
    .DATA_IDENTIFIER(4'h1),
    .BASEADDR(CLICFAST_RX_BASEADDR),
    .HIGHADDR(CLICFAST_RX_HIGHADDR)
) clicfast2023_rx (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .RX_CLK2x(CLK200),
    .RX_CLKW(CLK20),
    .RX_DATA(Data),

    .FPGA_TIMESTAMP(TIMESTAMP[47:0]),

    .FIFO_READ(RX_FIFO_READ),
    .FIFO_EMPTY(RX_FIFO_EMPTY),
    .FIFO_DATA(RX_FIFO_DATA),
    .ALIGNED(debug[0])
);

two_phase_spi #(
    .BASEADDR(SPI_CONF_BASEADDR),
    .HIGHADDR(SPI_CONF_HIGHADDR),
    .MEM_BYTES(128)
) two_phase_spi_conf (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(SPI_CLK),

    .SCLK1(CCk1),
    .SCLK2(CCk2),
    .SDO(CSOut),
    .SDI(CSIn),
    .SRB(CRb),
    .SLD(CLd),

    .EXT_START(1'b0),
    .SEN()
);
// ------- Switches ------- //
wire [15:0] GPIO_OUT;
gpio #( 
    .BASEADDR(GPIO_BASEADDR), 
    .HIGHADDR(GPIO_HIGHADDR),
    .IO_WIDTH(16),
    .IO_DIRECTION(16'hffff)
) gpio (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(GPIO_OUT)
);
assign RstAnalogB = GPIO_OUT[0];
assign EN_CkExt = GPIO_OUT[1];
assign EN_CkRef = GPIO_OUT[2];
assign SYNC_RES = GPIO_OUT[4];
assign EN_PULSE_SYNC = GPIO_OUT[5];
assign EN_Trigger = GPIO_OUT[6];
assign EN_CHOPPER = GPIO_OUT[7];

// -------  debug  ------- //
assign LED[0] = debug[0];
assign LED[1] = ~debug[1];
assign LED[2] = ~TS_INJ_FIFO_EMPTY;
assign LED[3] = ~TS_TRIG_FIFO_EMPTY;
assign LED[4] = ~RX_FIFO_EMPTY;
assign LED[7] =  GPIO_OUT[2];
endmodule
