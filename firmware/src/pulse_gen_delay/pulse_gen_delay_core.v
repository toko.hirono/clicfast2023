/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved 
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module pulse_gen_delay_core #(
    parameter ABUSWIDTH = 16
) (
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,
    
    input wire PULSE_CLK,
    input wire PULSE_CLKDLY,
    input wire EXT_START,
    output reg PULSE,
    output wire PULSE_DLY
);

localparam VERSION = 1;

wire SOFT_RST;
wire START;
reg CONF_EN;
reg CONF_DONE;
reg [5:0] CONF_EN_OUT;
reg [31:0] CONF_DELAY;
reg [31:0] CONF_WIDTH;
reg [31:0] CONF_REPEAT;
reg [4:0] CONF_FINE_DELAY;
reg CONF_FINE_DELAY_LD;
reg CONF_FINE_DELAY_CINV;
wire [4:0] FINE_DELAY_VALUE;

always@(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_EN_OUT, CONF_EN, CONF_DONE};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= CONF_DELAY[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= CONF_DELAY[15:8];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= CONF_DELAY[23:16];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= CONF_DELAY[31:24];
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= CONF_WIDTH[7:0];
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= CONF_WIDTH[15:8];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= CONF_WIDTH[23:16];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= CONF_WIDTH[31:24];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= CONF_REPEAT[7:0];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= CONF_REPEAT[15:8];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= CONF_REPEAT[23:16];
        else if(BUS_ADD == 14)
            BUS_DATA_OUT <= CONF_REPEAT[31:24];
        else if(BUS_ADD==15)
            BUS_DATA_OUT <= {1'b0, CONF_FINE_DELAY_CINV, CONF_FINE_DELAY_LD, CONF_FINE_DELAY};
        else if(BUS_ADD==16)
            BUS_DATA_OUT <= {3'b0, FINE_DELAY_VALUE};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end

assign SOFT_RST = (BUS_ADD==0 && BUS_WR);
assign START = (BUS_ADD==1 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 0;
        CONF_EN_OUT <= 6'b11_1111;
        CONF_DELAY <= 0;
        CONF_WIDTH <= 0;
        CONF_REPEAT <= 1;
        CONF_FINE_DELAY <= 5'b0;
        CONF_FINE_DELAY_LD <= 1'b0;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2)
            {CONF_EN_OUT, CONF_EN} <= BUS_DATA_IN[7:1];
        else if(BUS_ADD == 3)
            CONF_DELAY[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 4)
            CONF_DELAY[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 5)
            CONF_DELAY[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 6)
            CONF_DELAY[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 7)
            CONF_WIDTH[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 8)
            CONF_WIDTH[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 9)
            CONF_WIDTH[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 10)
            CONF_WIDTH[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 11)
            CONF_REPEAT[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 12)
            CONF_REPEAT[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 13)
            CONF_REPEAT[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 14)
            CONF_REPEAT[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 15)
            {CONF_FINE_DELAY_CINV, CONF_FINE_DELAY_LD, CONF_FINE_DELAY} <= BUS_DATA_IN[6:0];
    end
end


wire CONF_EN_SYNC;
three_stage_synchronizer conf_en_sync (
    .CLK(PULSE_CLK),
    .IN(CONF_EN),
    .OUT(CONF_EN_SYNC)
);

wire [31:0] CONF_DELAY_SYNC;
three_stage_synchronizer #(
    .WIDTH(32)
) conf_dely_sync (
    .CLK(PULSE_CLK),
    .IN(CONF_DELAY),
    .OUT(CONF_DELAY_SYNC)
);

wire [31:0] CONF_WIDTH_SYNC;
three_stage_synchronizer #(
    .WIDTH(32)
) conf_width_sync (
    .CLK(PULSE_CLK),
    .IN(CONF_WIDTH),
    .OUT(CONF_WIDTH_SYNC)
);

wire [31:0] CONF_REPEAT_SYNC;
three_stage_synchronizer #(
    .WIDTH(32)
) conf_repeat_sync (
    .CLK(PULSE_CLK),
    .IN(CONF_REPEAT),
    .OUT(CONF_REPEAT_SYNC)
);

wire RST_SYNC;
wire RST_SOFT_SYNC;
cdc_pulse_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(PULSE_CLK), .pulse_out(RST_SOFT_SYNC));
assign RST_SYNC = RST_SOFT_SYNC || BUS_RST;


wire START_SYNC;
cdc_pulse_sync start_pulse_sync (.clk_in(BUS_CLK), .pulse_in(START), .clk_out(PULSE_CLK), .pulse_out(START_SYNC));

wire EXT_START_SYNC;
reg [2:0] EXT_START_FF;
always @(posedge PULSE_CLK) // first stage
begin
    EXT_START_FF[0] <= EXT_START;
    EXT_START_FF[1] <= EXT_START_FF[0];
    EXT_START_FF[2] <= EXT_START_FF[1];
end

assign EXT_START_SYNC = !EXT_START_FF[2] & EXT_START_FF[1];

reg [31:0] CNT;

wire [32:0] LAST_CNT;
assign LAST_CNT = CONF_DELAY_SYNC + CONF_WIDTH_SYNC;

reg [31:0] REAPAT_CNT;

always @(posedge PULSE_CLK) begin
    if (RST_SYNC)
        REAPAT_CNT <= 0;
    else if(START_SYNC || (EXT_START_SYNC && CONF_EN_SYNC))
        REAPAT_CNT <= CONF_REPEAT_SYNC;
    else if(REAPAT_CNT != 0 && CNT == 1)
        REAPAT_CNT <= REAPAT_CNT - 1;
end

always @(posedge PULSE_CLK) begin
    if (RST_SYNC)
        CNT <= 0; //IS THIS RIGHT?
    else if(START_SYNC || (EXT_START_SYNC && CONF_EN_SYNC))
        CNT <= 1;
    else if(CNT == LAST_CNT && REAPAT_CNT != 0)
        CNT <= 1;
    else if(CNT == LAST_CNT && CONF_REPEAT_SYNC == 0)
        CNT <= 1;
    else if(CNT == LAST_CNT && REAPAT_CNT == 0)
        CNT <= 0;
    else if(CNT != 0)
        CNT <= CNT + 1;
end

always @(posedge PULSE_CLK) begin
    if(RST_SYNC || START_SYNC || (EXT_START_SYNC && CONF_EN_SYNC))
        PULSE <= 0;
    else if(CNT == CONF_DELAY_SYNC && CNT > 0)
        PULSE <= 1;
    else if(CNT == LAST_CNT)
        PULSE <= 0;
end

wire DONE;
assign DONE = (CNT == 0);

wire DONE_SYNC;
cdc_pulse_sync done_pulse_sync (.clk_in(PULSE_CLK), .pulse_in(DONE), .clk_out(BUS_CLK), .pulse_out(DONE_SYNC));

wire EXT_START_SYNC_BUS;
cdc_pulse_sync ex_start_pulse_sync (.clk_in(PULSE_CLK), .pulse_in(EXT_START && CONF_EN), .clk_out(BUS_CLK), .pulse_out(EXT_START_SYNC_BUS));

always @(posedge BUS_CLK)
    if(RST)
        CONF_DONE <= 1;
    else if(START || EXT_START_SYNC_BUS)
        CONF_DONE <= 0;
    else if(DONE_SYNC)
        CONF_DONE <= 1;

(* IODELAY_GROUP = "IODELAY_GROUP_Trigger" *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL
IDELAYCTRL IDELAYCTRL_trigger (
   .RDY(),          // 1-bit output: Ready output
   .REFCLK(PULSE_CLKDLY), // 1-bit input: Reference clock input
   .RST(BUS_RST)    // 1-bit input: Active high reset input
);
(* IODELAY_GROUP = "IODELAY_GROUP_Trigger" *)
IDELAYE2 #(
    .CINVCTRL_SEL("TRUE"),          // Enable dynamic clock inversion (FALSE, TRUE)
    .DELAY_SRC("DATAIN"),            // Delay input (IDATAIN, DATAIN)
    .HIGH_PERFORMANCE_MODE("TRUE"),  // Reduced jitter ("TRUE"), Reduced power ("FALSE")
    .IDELAY_TYPE("VAR_LOAD"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
    .IDELAY_VALUE(0),                // Input delay tap setting (0-31)
    .PIPE_SEL("FALSE"),              // Select pipelined mode, FALSE, TRUE
    .REFCLK_FREQUENCY(200.0),        // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
    .SIGNAL_PATTERN("DATA")         // DATA, CLOCK input signal
) IDELAYE2_i (
    .CNTVALUEOUT(FINE_DELAY_VALUE), // 5-bit output: Counter value output
    .DATAOUT(PULSE_DLY),               // 1-bit output: Delayed data output
    .C(BUS_CLK),                      // 1-bit input: Clock input
    .CE(1'b0),                        // 1-bit input: Active high enable increment/decrement input
    .CINVCTRL(CONF_FINE_DELAY_CINV),  // 1-bit input: Dynamic clock inversion input
    .CNTVALUEIN(CONF_FINE_DELAY),     // 5-bit input: Counter value input
    .DATAIN(PULSE),              // 1-bit input: Internal delay data input
    .IDATAIN(),                      // 1-bit input: Data input from the I/O
    .INC(),                          // 1-bit input: Increment / Decrement tap delay input
    .LD(CONF_FINE_DELAY_LD),                   // 1-bit input: Load IDELAY_VALUE input
    .LDPIPEEN(1'b0),                 // 1-bit input: Enable PIPELINE register to load data input
    .REGRST(BUS_RST)                 // 1-bit input: Active-high reset tap-delay input
);

endmodule
