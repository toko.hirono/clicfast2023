
=====================================
**pulse_gen_div** - pulse generator
=====================================

Pulse generator with configurable delay and width. Phase with respect to modlue clock is also variable.

**Unit test/Example:** 
`tests/test_PulseGenDiv.v`
`tests/test_PulseGenDiv.py`

Parameters
    +--------------+---------------------+-------------------------------------------------------------------------+ 
    | Name         | Default             | Description                                                             | 
    +==============+=====================+=========================================================================+ 
    | CLKDV        | 4                   | clock divisiator, CLKDIV = CLKDIV / PULSE_CLK in frequency              | 
    +--------------+---------------------+-------------------------------------------------------------------------+ 
    | OUTPUT_SIZE  | 2                   | Number of output channel                                                | 
    +--------------+---------------------+-------------------------------------------------------------------------+ 

Pins
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
    | Name           | Size                | Direction             | Description                                          | 
    +================+=====================+=======================+======================================================+ 
    | EXT_START      | 1                   |  input                | active high start signal (synchronous to PULSE_CLK)  | 
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
    | PULSE_CLK      | 1                   |  input                | module clock                                         | 
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
    | PULSE_CLKDIV   | 1                   |  input                | 1/2 of pahse clock                                   | 
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
    | PULSE_CLKDIV2x | 1                   |  input                | phase clock                                          | 
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
    | PULSE          | OUTPUT_SIZE         |  output               | output pulse                                         | 
    +----------------+---------------------+-----------------------+------------------------------------------------------+ 
  
Registers
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | Name          | Address                          | Bits   | r/w   | Default     | Description                                                                                | 
    +===============+==================================+========+=======+=============+============================================================================================+ 
    | START         | 1                                |        | wo    |             | software start on write to address                                                         | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | CLKDV         | 1                                |        | ro    | 4           | clock divisiator                                                                           | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | DONE          | 2                                | [0]    | ro    | 0           | indicate finish                                                                            | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | EN            | 2                                | [1]    | r/w   | 0           | enable external start                                                                      | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | EN_OUT        | 2                                | [7:2]  | r/w   | 0x3F        | enable/disable PULSE by channel                                                            | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | DELAY         | 6 - 3                            | [31:0] | r/w   | 0           | pulse delay from start                                                                     | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | WIDTH         | 10 - 7                           | [31:0] | r/w   | 0           | pulse width                                                                                | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | REPEAT        | 11 - 14                          | [31:0] | r/w   | 1           | repeat count (0 ->forever)                                                                 | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
    | PASE_DES      | 16                               | [7:0]  | r/w   | 1           | deserialized phase                                                                         | 
    +---------------+----------------------------------+--------+-------+-------------+--------------------------------------------------------------------------------------------+ 
