#!/bin/bash

if [ $# = 0 ]; then
   board="nexys"
else
   board=${1}
fi

if [ ${board} = "nexys" ]; then
    source ~/Xilinx/Vivado/2017.2/settings64.sh
    vivado -mode tcl -source make.tcl
else
    source ~/Xilinx/Vivado/2023.2/settings64.sh
    vivado -mode tcl -source make_${board}.tcl
fi