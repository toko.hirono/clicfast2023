#!/bin/bash

if [ $# = 0 ]; then
   board="nexys"
else
   board=${1}
fi
echo $board

if [ ${board} = "nexys" ]; then
    source ~/Xilinx/Vivado/2017.2/settings64.sh
    vivado -mode tcl -source program.tcl
elif [ ${board} = "gui" ]; then
    source ~/Xilinx/Vivado/2023.2/settings64.sh
    vivado &
else
    source ~/Xilinx/Vivado/2023.2/settings64.sh
    vivado -mode tcl -source program_genesys.tcl
fi