#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages

import clicfast2023
#from symbol import except_clause

import os

version = clicfast2023.__version__

author = 'T. Hirono (ADL IPE, KIT)'
author_email = 'toko.hirono@kit.edu'

# Requirements
install_requires = [#'basil-daq==3.2.0', 
                    #'bitarray>=2.0.0', 
                    #'matplotlib',
                    #'numpy', 
                    #'online_monitor>=0.4.0<0.5',
                    #'tables', 'pyyaml', 'pyzmq',
                    #'scipy', 'numba', 'tqdm'
                    ]
setup(
    name='clicfast2023-daq',
    version=version,
    description='DAQ for Clicfast2023',
    url=' ',
    license='',
    long_description='',
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    python_requires=">=3.0",
    packages=find_packages(),
    #setup_requires=['online_monitor>=0.4.0<0.5'],
    include_package_data=True,
    platforms='any',
)

try:
    from online_monitor.utils import settings
    # Get the absoulte path of this package
    package_path = os.path.dirname(clicfast2023.__file__)
    # Add online_monitor plugin folder to entity search paths
    settings.add_producer_sim_path(os.path.join(package_path,
                                                'online_monitor'))
    settings.add_converter_path(os.path.join(package_path,
                                             'online_monitor'))
    settings.add_receiver_path(os.path.join(package_path,
                                            'online_monitor'))
except ImportError:
    pass

