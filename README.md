# Clicfast2023-DAQ
[![Build Status]]()

DAQ for the [Clicfast2023](https://adl.ipe.kit.edu/english) chip based on the [Basil](https://github.com/SiLab-Bonn/basil) framework.

## Installation

- Install [miniconda](https://conda.io/miniconda.html) for python

- Install dependencies and clicfast2023-daq:
```
conda install numpy bitarray pyyaml scipy numba pytables matplotlib tqdm pyzmq blosc psutil
git clone https://github.com/SiLab-Bonn/basil.git
cd basil
python setup.py develop
# pip install git+https://git.scc.kit.edu/vt1680/clicfast2023-daq.git@master
git clone https://git.scc.kit.edu/vt1680/clicfast2023-daq.git@master
cd clicfast2023-daq
python setup.py develop
```

- Install tools (Optional)
```
conda install jupyter notebook
conda install sphinx
```

- Install vivado  (tested version = 2017.2)
Download from [Xilinx](https://www.xilinx.com/support/download.html) and install vivado.

## How to make document

```
cd docs
make singlehtml
firefox _build/singlehtml/index.html
```

## Setting up

- Complie bit file
```
cd firmware/vivado
./make.sh
./program.sh
```

- Connect all (see docs)

- set IP address of PC (IP address of the FPGA is 192.168.10.16)

- Power the chip


## Examples

see examples

